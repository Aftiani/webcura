<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Demo account                          _b80bac</name>
   <tag></tag>
   <elementGuidId>5d56ef7b-d9c8-490d-8304-89e6388c0165</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form.form-horizontal</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://katalon-demo-cura.herokuapp.com/authenticate.php']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>5aeba5de-a9a0-4701-a29b-510fd75c2d1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-horizontal</value>
      <webElementGuid>2517ea7b-f7e9-4523-aae0-5f08e439cfe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://katalon-demo-cura.herokuapp.com/authenticate.php</value>
      <webElementGuid>b4deea93-6d50-45be-acfd-c48345f36d80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>8d674d82-908f-49ff-a046-de89fab7c774</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                </value>
      <webElementGuid>1d0df558-2a64-4415-ad6e-983d71d7d805</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-3 col-sm-6&quot;]/form[@class=&quot;form-horizontal&quot;]</value>
      <webElementGuid>964fac63-bc33-4659-bfdf-99a2f0b1f055</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://katalon-demo-cura.herokuapp.com/authenticate.php']</value>
      <webElementGuid>90048d4a-e549-4166-b3e6-b6743df4b67a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div[2]/form</value>
      <webElementGuid>183dda66-110f-47e5-9772-105967141f9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::form[1]</value>
      <webElementGuid>e82a2fcb-ce35-458e-8647-b0966b3be5fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::form[1]</value>
      <webElementGuid>310c0a6d-0d66-4988-bf6b-93a85387286c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>922e0126-77b0-42ad-89db-eed214ad50c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                ' or . = '
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                ')]</value>
      <webElementGuid>1586c427-a4f6-4506-b105-a6b82d8d778d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
